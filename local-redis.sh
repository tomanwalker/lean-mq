#!/bin/bash

CONT_NAME=redis
IMAGE=redis
PORT_HOST=6379

docker run --name ${CONT_NAME} -d \
-p ${PORT_HOST}:6379 ${IMAGE} 

