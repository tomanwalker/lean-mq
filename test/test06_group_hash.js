
// ## dependencies
var path = require('path');
var expect = require('chai').expect;
var proxyquire = require('proxyquire');

var utils = require('./utils/common');

// ## config
var moduleName = path.basename(__filename);
var print = utils.getLogger(moduleName);

// ## target
var dirLevelUp = '../';
var unit = require(dirLevelUp + 'index.js');

// ## flow
describe(moduleName + '>> group with hash', function(){
	
	var c =null;
	var w = [];
	var count_workers = 3;
	
	it('init chain', function(){
		
		print('init queue - start...');
		c = new unit.Chain('test', {group: 'hash'});
		expect( c.name ).to.equal( 'test' );
		
	});
	
	it('define workers', function(){
		
		var prepare_worker = function(id){
			return function(obj){
				print(`worker ${id} - handle - obj = ${ JSON.stringify(obj) }`);
				
				w[id].res = w[id].res.concat( obj.payload.data );
				print(`worker ${id} - handle - before Resolve...`);
				return Promise.resolve(true);
			};
		};
		var prepare_hash = function(id){
			return function(obj){
				print('HASH debug = %j', obj);
			};
		};
		
		for(var i=0; i<count_workers; i++){
			w.push({ id: i, res:[] });
			
			c.attach( prepare_worker(i), prepare_hash(i) );
		}
		
		expect( w.length ).to.equal( count_workers );
		expect( w[0].res.length ).to.equal( 0 );
	});
	
	it('publish', function(done){
		
		var keys = [2,3,11,22,34,48,111,93420492];
		
		for(var z=0; z<keys.length; z++){
			var k = keys[z].toString();
			
			c.add(k, {key: k, smth: "abc"});
		}
		
		print(`publish - timeout BEFORE - ts = ${(new Date()).toISOString()}`);
		setTimeout(function(){
			
			print(`publish - timeout AFTER - ts = ${(new Date()).toISOString()}`);
			print(`publish - timeout - w = ${JSON.stringify(w)}`);
			expect( w[0].res.length ).to.equal( 1 );
			expect( w[1].res.length ).to.equal( 2 );
			expect( w[2].res.length ).to.equal( 5 );
			
			done();
			
		}, 1000);
	});
	
});


