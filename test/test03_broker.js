
// ## dependencies
var path = require('path');
var expect = require('chai').expect;
var proxyquire = require('proxyquire');

// ## config
var moduleName = path.basename(__filename);

// ## funcs
var print = function(msg){
	return console.log(moduleName + ' [TESTRUN] >> ' + msg);
};

// ## target
var dirLevelUp = '../';
var unit = require(dirLevelUp + 'index.js');

// ## flow
describe(moduleName + '>> broker positive', function(){
	
	var broker = null;
	var pub = null;
	var subs = [];
	var cnt_workers = 2;
	var cnt_msg = 2;
	var topic = 'test';
	
	var received = {};
	var ts_pub = null;
	var ts_got = null;
	
	it('init', function(done){
		
		print('init - start...');
		
		broker = new unit.Broker({test:"abc"});
		
		print('init - broker.opts = ' + JSON.stringify(broker.opts) );
		expect(broker.opts.test).to.equal("abc");
		expect(broker.opts.port).to.equal(2525);
		
		broker.add( topic, 'chain', {window:1000} ).then(function(result){
			
			expect( broker.topics[topic].name ).to.equal( topic );
			done();
		});
		
	});
	
	it('sub', function(done){
		
		print('sub - start...');
		
		var make_client = function(id){
			var func = function(topic, msg, headers){
				print(`sub [${id}] - received - topic = ${topic} | `
					+ `msg = ${JSON.stringify(msg)} | headers = ${JSON.stringify(headers)}`);
				
				ts_got = new Date();
				
				if( !received[id] ){
					received[id] = [];
				}
				
				received[id].push(msg);
				
			};
			
			var client = new unit.Client({name: 'worker-' + id});
			
			client.connect('localhost:2525', function(){
				
				print('connected = ' + JSON.stringify( client.opts ));
				client.opts.ok = true;
				
				client.subscribe(topic, func);
				
			});
			
			return client;
			
		};
		
		setTimeout(function(){
			
			print('--------------');
			
			for(var i=0; i<cnt_workers; i++){
				var id = i + 1;
				
				var x = make_client(id);
				subs.push( x );
				
			}
			
			expect( subs.length ).to.equal(cnt_workers);
			
			print('--------------');
			
			setTimeout(function(){
				
				print('check = ' + JSON.stringify(subs[0].opts));
				expect( subs[0].opts.ok ).to.equal( true );
				
				print('broker.subs = ' + JSON.stringify( Object.keys(broker.subs) ));
				//print('broker.subs = ' + JSON.stringify( broker.subs ));
				expect( broker.subs[topic].clients.length ).to.equal(cnt_workers);
				
				done();
				
			}, 100);
			
		}, 100);
	});
	
	it('publish', function(done){
		
		this.timeout(3000);
		print('publish - start...');
		
		setTimeout(function(){
			
			pub = new unit.Client({name: 'pub-1'});
			pub.connect('localhost:2525', function(){
				
				print('pub.connected = ' + JSON.stringify( pub.opts ));
				
				//var cnt_workers = 2;
				//var cnt_msg = 2;
				var cnt = 1;
				
				for(var k=0; k<cnt_msg; k++){
					for(var m=0; m<cnt_workers; m++){
						
						var key = (m+1);
						var data = {id:(cnt++), text:'abc'};
						
						pub.send(topic, data, key);
						
					}
				}
				
				ts_pub = new Date();
				
				setTimeout(function(){
					
					var diff = ts_got - ts_pub; // between last pub & last receive
					
					print('pub.timeout - elapsed - val = (2000) | diff = ' + diff);
					
					var rec_counter = 0;
					for(var p in received){
						
					}
					
					//var cnt_workers = 2;
					//var cnt_msg = 2;
					expect( Object.keys(received).length ).to.equal( cnt_workers );
					for(var p in received){
						
						expect( received[p].length ).to.equal(1); // one message per listener
						expect( received[p][0].length ).to.equal( cnt_msg );
						
					}
					
					// connection close here for all 3
					pub.close();
					for(var i=0; i<cnt_workers; i++){
						
						subs[i].close();
					}
					
					return done();
					
				}, 2000);
				
			});
			
		}, 400);
		
	});
	
});

describe(moduleName + '>> client negative', function(){
	
	var neg = null;
	
	it('missing opts', function(done){
		var client = new unit.Client( null );
		
		expect( client.opts.name.length ).to.equal( 5 ); // assigned random
		done();
		
	});
	
	it('missing url & callback', function(done){
		
		var client = new unit.Client( {name: 'test', wrong:true } );
		
		expect( Object.keys(client.opts).length, 
			`client.opts (${JSON.stringify(client.opts)}) should have 2 props`
		).to.equal(2);
		
		client.connect();
		expect( client.ws !== null ).to.be.true;
		
		client.ws.on('error', function(err) {});
		done();
		
	});
	/*
	it('missing topic & callback', function(done){
		
		var client = new unit.Client( {name: 'test', wrong:true } );
		client.connect();
		
		expect( Object.keys(client.opts).length ).to.equal(2);
		expect( client.ws !== null ).to.be.true;
		
		client.subscribe();
		done();
		
	});
	*/
	
});


