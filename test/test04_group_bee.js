
// ## dependencies
var path = require('path');
var expect = require('chai').expect;
var proxyquire = require('proxyquire');

// ## config
var moduleName = path.basename(__filename);
var redis_host = process.env.REDIS_HOST || 'localhost';

// ## funcs
var print = function(msg){
	return console.log(moduleName + ' [TESTRUN] >> ' + msg);
};

// ## target
var dirLevelUp = '../';
var unit = require(dirLevelUp + 'index.js');

// ## flow
describe(moduleName + '>> group redis', function(){
	
	var c = null;
	var count_groups = 2;
	var count_msg_per_group = 2;
	var count_workers = 2;
	var w = [];
	
	it('init queue', function(done){
		
		print('init queue - start...');
		
		var qOpts = {window: 500, storage: 'redis', connection:{host: redis_host} };
		c = new unit.Chain('test', qOpts);
		
		expect( c.name ).to.equal( 'test' );
		done();
		
	});
	
	it('define N workers', function(done){
		
		var prepare_worker = function(id){
			return function(obj){
				var date = new Date();
				print('=============');
				print(`worker ${id} - handle - obj = ${ JSON.stringify(obj) }`);
				
				w[id].res = obj.payload.data;
				w[id].ts = date.toISOString();
				print(`worker ${id} - handle - before Resolve - ${w[id].ts}`);
				print('=============');
				return Promise.resolve(true);
			};
		};
		
		for(var i=0; i<count_workers; i++){
			
			w.push( {id: i, res:[] } );
			
			var f = prepare_worker(i);
			c.attach( f );
			
		}
		
		expect( w.length ).to.equal( count_workers );
		expect( w[0].res.length ).to.equal( 0 );
		
		done();
		
	});
	
	it('publish', function(done){
		
		this.timeout(3000);
		
		var start = new Date();
		print(`publish - start = ${start.toISOString()}`);
		var id = 1;
		
		for(var x=0; x<count_msg_per_group; x++){
			for(var z=1; z<=count_groups; z++){
				
				c.add( z.toString(), { key: z, id: (id++), data:"abc" } );
				
			}
		}
		
		setTimeout(function(){
			
			var end = new Date();
			print(`publish - w = ${JSON.stringify(w)} | diff = ${end-start} | `
				+ `start = ${start.toISOString()} | end = ${end.toISOString()}`
			);
			
			expect( w[0].res !== null ).to.equal( true );
			expect( w[0].res.length ).to.equal( count_msg_per_group );
			
			done();
			
		}, 1000);
		
	});
	
});


