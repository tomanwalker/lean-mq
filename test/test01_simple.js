
// ## dependencies
var path = require('path');
var expect = require('chai').expect;
var proxyquire = require('proxyquire');

var utils = require('./utils/common');

// ## config
var moduleName = path.basename(__filename);
var print = utils.getLogger(moduleName);

// ## funcs

// ## target
var dirLevelUp = '../';
var unit = require(dirLevelUp + 'index.js');

// ## flow
describe(moduleName + '>> simple in memory', function(){
	
	var q = null;
	var total = 4;
	var res = [];
	
	var eventResults = [];
	
	it('init queue', function(done){
		
		print('init queue - start...');
		q = new unit.Queue('test');
		
		expect( q.name ).to.equal( 'test' );
		
		done();
		
	});
	
	it('init worker', function(done){
		
		print('init worker - start...');
		q.handle(function(msg){
			print('handle - msg = ' + JSON.stringify(msg) );
			res.push( msg.payload );
			
			return Promise.resolve(true);
		});
		
		done();
		
	});
	
	it('sub events', function(done){
		
		print('sub events - start...');
		
		q.on('new', function(obj){
			print('on.new - obj = %j', obj);
			eventResults.push({kind: 'new', key: obj.key});
		});
		q.on('completed', function(obj){
			print('on.completed - obj = %j', obj);
			eventResults.push({kind: 'completed', id: obj.id});
		});
		
		done();
	});
	
	it('publish', function(done){
		
		print('publish - start...');
		
		expect( res.length ).to.equal(0);
		
		for(var z=1; z<=total; z++){
			var msg = { key: z, data:"abc" };
			q.add( msg );
		}
		
		setTimeout(function(){
			
			print('publish - done - res =' + JSON.stringify(res) );
			print('publish - done - events =' + JSON.stringify(eventResults) );
			
			expect( res.length ).to.equal(total);
			expect( eventResults.length ).to.equal(total);
			
			expect( res[0].data ).to.equal("abc");
			expect( eventResults[0].kind ).to.equal("new");
			
			done();
			
		}, 150);
		
	});
	
	
	
});

describe(moduleName + '>> simple negative', function(){
	
	it('async.fail', function(done){
		
		print('init queue - start...');
		var t = new unit.Queue('test-fail', {attempts: 2, retry_delay: 20});
		
		t.handle(function(obj){
			return new Promise(function(resolve, reject){
				print('handle - obj = %j', obj);
				setTimeout(function(){
					return reject('something went worng');
				}, 50);
			});
		});
		
		t.add('hi').then(function(result){
			print('add.then - result = %j', result);
		});
		
		setTimeout(done, 300);
		
	});
});


