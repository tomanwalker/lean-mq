
// ## dependencies
var path = require('path');
var expect = require('chai').expect;
var proxyquire = require('proxyquire');

// ## config
var moduleName = path.basename(__filename);

// ## funcs
var print = function(...msg){
	msg[0] = moduleName + ' [TESTRUN] >> ' + msg[0];
	return console.log(...msg);
};

// ## target
var dirLevelUp = '../';
var unit = require(dirLevelUp + 'index.js');

// ## flow
describe(moduleName + '>> group in memory', function(){
	
	var c = null;
	var count_groups = 2;
	var count_msg_per_group = 2;
	var count_workers = 2;
	var w = [];
	
	it('init queue', function(done){
		
		print('init queue - start...');
		c = new unit.Chain('test', {window: 200});
		
		expect( c.name ).to.equal( 'test' );
		
		done();
		
	});
	
	it('define N workers', function(done){
		
		var prepare_worker = function(id){
			return function(obj){
				print(`worker ${id} - handle - obj = ${ JSON.stringify(obj) }`);
				
				w[id].res = obj.payload.data;
				print(`worker ${id} - handle - before Resolve...`);
				return Promise.resolve(true);
			};
		};
		
		for(var i=0; i<count_workers; i++){
			
			w.push( {id: i, res:[] } );
			
			var f = prepare_worker(i);
			c.attach( f );
			
		}
		
		expect( w.length ).to.equal( count_workers );
		expect( w[0].res.length ).to.equal( 0 );
		
		done();
		
	});
	
	it('publish', function(done){
		
		var id = 1;
		
		for(var x=0; x<count_msg_per_group; x++){
			for(var z=1; z<=count_groups; z++){
				
				print(`publish - before - key = ${z} | id = ${id}`);
				
				c.add( z.toString(), { key: z, id: (id++), data:"abc" } ).then(function(result){
					print('publish - after - res = ' + result);
				});
				
			}
		}
		
		print(`publish - timeout BEFORE - ts = ${(new Date()).toISOString()}`);
		setTimeout(function(){
			
			print(`publish - timeout AFTER - ts = ${(new Date()).toISOString()}`);
			print(`publish - timeout - w = ${JSON.stringify(w)}`);
			expect( w[0].res.length ).to.equal( count_msg_per_group );
			
			done();
			
		}, 500);
		
	});
	
});

describe(moduleName + '>> grouping - negative', function(){
	
	var instance = null;
	var worker = null;
	
	it('neg.init', function(done){
		
		print('chain.neg - init - start...');
		
		worker = function(obj){
			print('chain.neg - intentionally fail - obj = ' + JSON.stringify(obj));
			return Promise.reject('smth went wrong');
		};
		
		var chainOpts = {
			'window': 50,
			retry_delay: 50
		};
		instance = new unit.Chain('test', chainOpts);
		instance.attach(worker);
		
		expect( instance.name ).to.equal( 'test' );
		done();
		
	});
	
	it('neg.publish', function(done){
		
		print('chain.neg - publish - start...');
		var invoked = 0;
		
		instance.on('error', function(err){
			print('chain.neg - chain err event - err = ' + JSON.stringify(err));
			
			expect( err.response ).to.equal( 'smth went wrong' );
			expect( err.msg ).to.equal( "worker rejected" );
			
			invoked += 1;
		});
		
		instance.add('a', { data:'abc' });
		
		setTimeout(function(){
			
			print('chain.neg - timeout - invoked = ' + invoked);
			expect( invoked > 0 ).to.equal( true );
			done();
			
		}, 500);
	});
	
});

describe(moduleName + '>> group.memory - delay workers', function(){
	
	var chain = null;
	
	it('init queue', function(done){
		
		print('init queue - start...');
		chain = new unit.Chain('test', {window: 200});
		
		expect( chain.name ).to.equal( 'test' );
		done();
		
	});
	
	it('publish first', function(){
		
		var date = new Date();
		var ts = date.toISOString();
		
		///function(key, obj)
		chain.add( "abc", { key: "1", data:"abc", ts: ts} ).then(function(result){
			print('publish - after - res = ' + result);
		});
		/*
		chain.add( "abc", { key: "2", data:"dfjeee", ts: ts } ).then(function(result){
			print('publish - after - res = ' + result);
		});
		*/
		
	});
	
	it('later workers', function(done){
		
		this.timeout(2000);
		var got = 0;
		
		setTimeout(function(){
			
			print('workers - connect delay...');
			
			chain.attach(function(obj){
				
				var gotDate = new Date();
				print('worker- handle - ts = %s | obj = %j', gotDate.toISOString(), obj);
				got += 1;
				
				return Promise.resolve(true);
			});
			
			setTimeout(function(){
				
				print('workes expect check - got = %s', got);
				expect( got > 0 , 'got = ' + got).to.be.true;
				done();
				
			}, 1000);
			
		}, 300);
		
	});
	
});

describe(moduleName + '>> group.memory - async job', function(){
	var chain = null;
	
	var restStub = function(){
		return new Promise(function(resolve, reject){
			setTimeout(function(){
				
				return resolve('123');
				
			}, 50);
		});
	};
	
	it('init queue', function(done){
		
		print('init queue - start...');
		chain = new unit.Chain('test', {window: 200});
		
		expect( chain.name ).to.equal( 'test' );
		done();
		
	});
	
	it('attach & publish', function(done){
		chain.attach(async function(obj){
			
			var start = new Date();
			var res = await restStub();
			var end = new Date();
			
			print('worker - handle - res = %s | diff = %s | start = %s', res, end-start, start);
			return true;
		});
		
		///function(key, obj)
		chain.add( "mykey", {data:"abc"}).then(function(result){
			print('publish - after - res = ' + result);
		});
		
		setTimeout(function(){
			print('async.timeout....');
			done();
		}, 400);
	});
});


