
// ## dependencies

// ## config

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.getLogger = function(moduleName){
	return function(...msg){
		msg[0] = moduleName + ' [TESTRUN] >> ' + msg[0];
		return console.log(...msg);
	};
};


