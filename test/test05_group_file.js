
// ## dependencies
var path = require('path');
var expect = require('chai').expect;
var proxyquire = require('proxyquire');

// ## config
var moduleName = path.basename(__filename);
var file_prefix = process.env.FILE_PREFIX || 'db';

// ## funcs
var print = function(...msg){
	msg[0] = moduleName + ' [TESTRUN] >> ' + msg[0];
	return console.log(...msg);
};

// ## target
var dirLevelUp = '../';
var unit = require(dirLevelUp + 'index.js');

// ## flow
describe(moduleName + '>> group file', function(){
	
	var c = null;
	var count_groups = 2;
	var count_msg_per_group = 2;
	var count_workers = 2;
	var w = [];
	
	it('init queue', function(done){
		
		print('init queue - start...');
		c = new unit.Chain('test', {window: 200, storage: 'file', connection:{name: file_prefix} });
		
		expect( c.name ).to.equal( 'test' );
		done();
		
	});
	
	it('define N workers', function(done){
		
		var prepare_worker = function(id){
			return function(obj){
				print(`worker ${id} - handle - obj = ${ JSON.stringify(obj) }`);
				
				w[id].res = obj.payload.data;
				print(`worker ${id} - handle - before Resolve...`);
				return Promise.resolve(true);
			};
		};
		
		for(var i=0; i<count_workers; i++){
			
			w.push( {id: i, res:[] } );
			
			var f = prepare_worker(i);
			c.attach( f );
			
		}
		
		expect( w.length ).to.equal( count_workers );
		expect( w[0].res.length ).to.equal( 0 );
		
		done();
		
	});
	
	it('publish', function(done){
		
		var id = 1;
		
		for(var x=0; x<count_msg_per_group; x++){
			for(var z=1; z<=count_groups; z++){
				
				c.add( z.toString(), { key: z, id: (id++), data:"abc" } );
				
			}
		}
		
		setTimeout(function(){
			
			print(`publish - w = ${JSON.stringify(w)}`);
			expect( w[0].res.length ).to.equal( count_msg_per_group );
			
			done();
			
		}, 500);
		
	});
	
	it('verify file', function(done){
		this.timeout(3000);
		
		setTimeout(function(){
			
			var fs = require('fs');
			//var data = fs.readFileSync('../' + file_prefix); 
			//print('file = %s', data); 
			// TODO list Files
			
			done();
			
		}, 1000);
		
	});
	
});


