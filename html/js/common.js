
var req = async function(method, url, body){
	return new Promise(function(resolve, reject){
		var opts = {
			type: method,
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(data){
				resolve(data);
			},
			error: function(err) {
				reject(err);
			}
		};
		if( body ){
			opts.data = JSON.stringify(body);
		}
		
		$.ajax(opts);
	});
};

var postPromise = function(url, body){
	return req('POST', url, body);
};
var getPromise = function(url){
	return req('GET', url);
};



