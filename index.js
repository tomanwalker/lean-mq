"use strict";

// Alex Shkunov - 2020 - Initial implmentation
// --"-- - 2022 - Additional features

// ## dependencies
var events = require('events');
var log = require('debug')('lean-mq');

var module_queue = require('./lib/queue');
var module_chain = require('./lib/chain');
var module_broker = require('./lib/broker');
var module_client = require('./lib/client');
var module_dashboard = require('./lib/dashboard');

// ## config

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.Queue = module_queue.Queue;
ns.Chain = module_chain.Chain;
ns.Broker = module_broker.Broker;
ns.Client = module_client.Client;

// ## flow
// if started directly not as Lib - init dashboard
if (require.main === module) {
	ns.dir = __dirname;
	module_dashboard.init(ns);
}


