
// ## dependencies
var log = require('debug')('lean-mq');
var fs = require('fs');

// ## config

// ## export
module.exports = function(name, opts){
	
	log('Storage::file-table.init - start - name =', name, '| opts =', opts);
	
	// ### props
	var _this = this;
	this.name = name;
	this.folder = './data';
	this.save_interval = 1;
	
	this.data = {};
	
	// ### init
	if( opts.connection && opts.connection.interval ){
		this.save_interval = opts.connection.interval;
	}
	
	this.path = this.folder + '/' + this.name + '-table.json';
	
	try{
		var oldDataText = fs.readFileSync(this.path);
		_this.data = JSON.parse( oldDataText );
	}
	catch(e){
		
	}
	
	// periodic save
	setInterval(function(){
		
		log('Storage::file-table - persist - name = %s | keys = %s', _this.name, Object.keys(_this.data).length );
		if( !fs.existsSync(_this.folder) ){
			fs.mkdirSync(_this.folder);
		}
		
		var dataText = JSON.stringify(_this.data, null, 2);
		fs.writeFileSync(_this.path, dataText);
		
	}, _this.save_interval * 1000);
	
	// ### methods
	this.get = function(key){
		
		return Promise.resolve( _this.data[key] );
	};
	
	this.set = function(key, val){
		
		log('Storage::file-table.set - start - name = %s | key = %s', name, key);
		_this.data[key] = val;
		return Promise.resolve( true );
	};
	
	this.del = function(key){
		
		delete _this.data[key];
		return Promise.resolve( true );
	};
	
};


