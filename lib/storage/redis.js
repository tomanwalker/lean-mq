
// ## dependencies
var Bee = require('bee-queue');
var log = require('debug')('lean-mq');

// ## config
var redis = null;

// ## export
module.exports = function(name, arg_opts){
	
	log('Storage::bee.init - start - name =', name, '| opts =', arg_opts);
	
	// ### props
	var _this = this;
	this.data = null;
	this.opts = {
		prefix: 'lmq',
		activateDelayedJobs: true
	};
	this.func = null;
	
	// ### config
	if( arg_opts.connection ){
		this.opts.redis = arg_opts.connection;
	}
	this.data = new Bee( name, this.opts );
	
	this.data.process(async function(job){
		if( _this.func === null ){
			return Promise.reject('no func defined');
		}
		
		return await _this.func( job.data );
	});
	
	// ### methods
	this.add = function(obj, delay){
		var job = _this.data.createJob(obj);
		var delayToUse = delay || obj.delay;
		log('Storage::bee.add - start - name =', name, '| delay =', delayToUse);
		
		if( delayToUse ){
			var date = new Date();
			var future = new Date(date.getTime() + delayToUse);
			
			job.delayUntil( future );
		}
		
		return job.save();
	};
	
	this.handle = function(arg_func){
		_this.func = arg_func;
	}
	
};


