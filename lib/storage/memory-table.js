
// ## dependencies
var log = require('debug')('lean-mq');

// ## config

// ## export
module.exports = function(name, opts){
	
	log('Storage::memory-table.init - start - name =', name, '| opts =', opts);
	
	// ### props
	var _this = this;
	this.data = {};
	
	// ### init
	
	// ### methods
	this.get = function(key){
		
		return Promise.resolve( _this.data[key] );
	};
	
	this.set = function(key, val){
		
		log('Storage::memory-table.set - start - name = %s | key = %s', name, key);
		_this.data[key] = val;
		return Promise.resolve( true );
	};
	
	this.del = function(key){
		
		delete _this.data[key];
		return Promise.resolve( true );
	};
	
};


