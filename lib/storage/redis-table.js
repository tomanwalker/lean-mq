
// ## dependencies
var log = require('debug')('lean-mq');
var Redis = require('ioredis');

// ## config

// ## export
module.exports = function(arg_name, arg_opts){
	
	log('Storage::redis-table.init - start - name =', arg_name, '| opts =', arg_opts);
	
	// ### props
	var _this = this;
	this.name = arg_name;
	this.opts = arg_opts;
	this.conn = null;
	
	// ### init
	this.conn = new Redis(arg_opts.connection);
	
	this.conn.on('connect', function(){
		log('Storage::redis-table - on.connect - established...');
	});
	
	// ### methods
	this.get = async function(key){
		log('Storage::redis-table - get - name = %s | key = %s', _this.name, key);
		var k = _this.name + '-' + key;
		var val = await _this.conn.get(k);
		log('Storage::redis-table - get - k = %s | val = %s', k, val);
		return JSON.parse(val);
		
	};
	
	this.set = async function(key, val){
		log('Storage::redis-table - set - name = %s | key = %s', _this.name, key);
		var k = _this.name + '-' + key;
		var str = JSON.stringify(val);
		var result = await _this.conn.set(k, str, "EX", _this.opts.window * 2);
		return result;
		
	};
	
	this.del = async function(key){
		var k = _this.name + '-' + key;
		return await _this.conn.del(k);
	};
	
};


