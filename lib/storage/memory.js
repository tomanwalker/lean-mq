
// ## dependencies
var events = require('events');
var log = require('debug')('lean-mq');

// ## config

// ## export
module.exports = function(name, opts){
	
	log('Storage::simple.init - start - name =', name, '| opts =', opts);
	
	// ### props
	var _this = this;
	this.name = name;
	this.data = [];
	
	this.opts = opts;
	this.retry_num = opts.attempts;
	this.retry_time = opts.retry_delay;
	
	this.func = null;
	this.running = false;
	
	var emitter = new events.EventEmitter();
	
	// ### config
	var gonext = async function(){
		
		_this.running = true;
		if( _this.data.length === 0 ){
			//log('Storage::simple.event.add - empty - name = %s | len = %s', _this.name, _this.data.length );
			_this.running = false;
			return true;
		}
		
		try {
			log('Storage::simple.event.add - start - name = %s | len = %s', _this.name, _this.data.length );
			var obj = _this.data.shift();
			var result = await _this.func( obj );
			log('Storage::simple.event.add - then - name =', _this.name, '| res =', result );
			
		}
		catch(err){
			log('Storage::simple.event.add - catch - name =', _this.name, '| err =', err );
			
			if( typeof(obj.retry) === 'undefined' ){
				obj.retry = 0;
			}
			
			if( err !== 'no workers attached' ){
				log('Storage::simple.event.add - retry - name =', _this.name, '| retry =', obj.retry );
				obj.retry += 1;
			}
			if( obj.retry < _this.retry_num ){
				
				var delay = (_this.retry_time * (obj.retry+1));
				_this.add(obj, delay);
			}
		}
		
		return await gonext();
	};
	
	var onadd = function(){
		
		if( _this.running ){
			return false;
		}
		
		return gonext();
	};
	
	//emitter.on('add', onadd);
	
	// ### methods
	this.add = function(obj, arg_delay){
		
		var delay_ms = arg_delay || obj.delay || 0;
		
		setImmediate(function(){
			log('Storage::simple.add - start - name =', _this.name, '| delay =', delay_ms );
			_this.data.push(obj);
			setTimeout(onadd, delay_ms);
		});
		
		return Promise.resolve( true );
		
	};
	
	this.handle = function(func){
		_this.func = func;
	}
	
};
