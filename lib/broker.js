"use strict";

// ## dependencies
var log = require('debug')('lean-mq');
var WebSocket = require('ws');
var kinds = {
	'queue': require('./queue').Queue,
	'chain': require('./chain').Chain
};

// ## config
var defaults = {
	test: true,
	port: 2525
};

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.Broker = function(opts){
	log('Broker.init - start - opts =', opts);
	
	// ### props
	var _this = this;
	
	this.opts = Object.assign({}, defaults);
	this.clients = [];
	this.topics = {};
	this.subs = {};
	this.wss = null;
	
	// ### config
	if( opts ){
		for(var p in opts){
			if( this.opts[p] ){
				this.opts[p] = opts[p];
			}
		}
	}
	
	log('Broker.init - starting WebSocket server - port = %s', this.opts.port);
	
	this.wss = new WebSocket.Server({ port: this.opts.port });
	this.clients = this.wss.clients;
	/*
	 * wss.clients - array contains clients
	 * client.readyState === WebSocket.OPEN - client is active
	 */
	
	
	this.wss.on('connection', function(socket) {
		
		log('Broker.on - connect - new client connected...');
		
		
		socket.on('message', function(msg) {
			log('Broker.on - message - msg = %s', msg);
			msg = JSON.parse(msg);
			
			if( msg.topic === '_conn' ){
				socket.socket_id = msg.body;
			}
			else if( msg.topic === '_sub' ){
				
				if( !_this.subs[ msg.body ] ){
					_this.subs[ msg.body ] = {
						robin: 0,
						clients: []
					};
				}
				
				_this.subs[ msg.body ].clients.push({
					name: socket.socket_id,
					socket: socket,
					ts: new Date()
				});
				
				log('Broker.on - message _sub - subs = %j', Object.keys(_this.subs) );
				
			}
			else if( msg.topic === '_close' ){
				
				for(var t in _this.subs){
					for(var c in _this.subs[t].clients){
						if( msg.body === _this.subs[t].clients[c].name ){
							log('Broker.on - message _close - name = %s | topic = %s', msg.body, t );
							_this.subs[t].clients.splice(c, 1);
							log('Broker.on - message _close  - topic = %s | subs.left = %s', t, _this.subs[t].clients.length );
						}
					}
				}
				
			}
			else {
				
				if( !_this.subs[ msg.topic ] ){
					log('Broker.on - error: topic not defined - name = %j', msg.topic );
					return false;
				}
				
				// Chain = this.add = function(key, obj);
				// Queue = this.add = function(obj);
				
				if( _this.topics[ msg.topic ].kind === 'queue' ){
					_this.topics[ msg.topic ].obj.add(msg.body);
				}
				if( _this.topics[ msg.topic ].kind === 'chain' ){
					_this.topics[ msg.topic ].obj.add(msg.headers.key, msg.body);
				}
				
			}
			
		});
		
	});
	
	// ### methods
	var distribute = function(topic){
		return function(obj){
			log('Broker.distribute - start - topic = %s | obj = %j', topic, obj);
			
			if( _this.subs[ topic ].clients.length === 0 ){
				// no listeners
				log('Broker.distribute - error - topic = %s | err = %s', topic, 'no listeners');
				return Promise.resolve( true );
				
			}
			
			log('Broker.distribute - clients = %s | robin.before = %s', 
				_this.subs[ topic ].clients.length, _this.subs[ topic ].robin);
			
			if( !_this.subs[ topic ].clients[ _this.subs[ topic ].robin ] ){
				_this.subs[ topic ].robin = 0;
			}
			
			log('Broker.distribute - send - topic = %s | robin = %s', topic, _this.subs[ topic ].robin);
			
			var msg = JSON.stringify({
				topic: topic,
				body: obj.payload.data,
				headers: {
					key: obj.payload.key
				}
			});
			_this.subs[ topic ].clients[ _this.subs[ topic ].robin++ ].socket.send(msg);
			
			return Promise.resolve( true );
		};
	};
	
	this.add = function(name, kind, other){
		
		log('Broker.add - start - name = %s | kind = %s | other = %s', name, kind, other);
		
		// validate
		if( _this.topics[ name ] ){
			log('Broker.add - error: already exist - name = %s', name);
			return Promise.reject( 'error: already exist - name = ' + name );
		}
		if( typeof(kind) === 'undefined' || !kinds[kind] ){
			kind = 'queue';
		}
		
		// setup
		_this.topics[ name ] = {
			name: name,
			kind: kind,
			obj: new kinds[kind](name, other),
			opts: other
		};
		
		_this.topics[name].obj.handle(distribute(name));
		
		return Promise.resolve( true );
		
	};
	
};


