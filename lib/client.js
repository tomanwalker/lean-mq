"use strict";

// ## dependencies
var log = require('debug')('lean-mq');
var WebSocket = require('ws');

// ## config
var defaults = {
	name: 'none',
	url: 'localhost:2525'
};
var RANDOM_LEN = 5;

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.random_string = function(len){
	var result = '';
	var characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
	var charactersLength = characters.length;
	for ( var i = 0; i < len; i++ ) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	
	return result;
	
};

ns.dummy = function(parent){
	return function(){
		log('Client.dummy - %s - not function was provided', parent);
		return false;
	};
};

ns.Client = function(arg_opts){
	log('Client.init - start - opts =', arg_opts);
	
	// props
	this.opts = Object.assign({}, defaults);
	var _this = this;
	this.ws = null;
	this.handler = ns.dummy('handler');
	
	// config
	if( arg_opts ){
		for(var p in this.opts){
			if( typeof(arg_opts[p]) !== 'undefined' ){
				this.opts[p] = arg_opts[p];
			}
		}
	}
	if( this.opts.name === 'none' ){
		this.opts.name = ns.random_string(5);
	}
	
	log('Client.init - opts populated - opts =', this.opts);
	
	// func
	this.send = function(topic, body, key){
		
		log('Client.send - sending - topic = %s | data = %j', topic, body);
		
		var msg = {
			topic: topic,
			body: body
		};
		
		if( topic[0] !== '_' ){
			msg.headers = {
				key: ( key || 'none' )
			};
		}
		
		return _this.ws.send(JSON.stringify( msg ));
		
	};
	
	this.connect = function(url, callback){
		
		if(typeof(url) === 'undefined'){
			url = _this.opts.url;
		}
		if( typeof(callback) === 'undefined' ){
			callback = ns.dummy('connect');
		}
		
		log('Client.connect - start - url = %s', url);
		
		_this.ws = new WebSocket('ws://' + url);
		
		_this.ws.on('open', function(){
			log('Client.on - open - name = %s', _this.opts.name);
			
			_this.send('_conn', _this.opts.name);
			
			callback();
		});
		
		_this.ws.on('message', function(data){
			log('Client.on - message - data = %s', data);
			if( _this.handler ){
				data = JSON.parse(data);
				return _this.handler(data.topic, data.body, data.headers);
			}
		});
		
	};
	
	this.subscribe = function(topic, func){
		
		log('Client.sub - name = %s | topic = %s', _this.opts.name, topic);
		
		if( func ){
			_this.handler = func;
		}
		_this.send('_sub', topic);
		
		return true;
	};
	
	this.close = function(){
		
		log('Client.close - name = %s', _this.opts.name);
		_this.send('_close', _this.opts.name);
		
		_this.ws.close();
		return true;
	};
	
};


