
// ## dependencies
var events = require('events');
var log = require('debug')('lean-mq');

// ## config

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.Queue = function(arg_name, arg_opts){
	
	// ### props
	var _this = this;
	this.name = arg_name;
	this.opts = {
		storage: 'memory', // memory, redis
		connection: null,
		attempts: 3,
		retry_delay: 1000
	};
	
	this.storage = null;
	this.eventEmitter = new events.EventEmitter();
	
	// ### config
	
	// TODO opts validation
	if( arg_opts ){
		for(var p in this.opts){
			if( typeof(arg_opts[p]) !== 'undefined' ){
				this.opts[p] = arg_opts[p];
			}
		}
	}
	
	log('Queue.init - start - name = %s | opts = %j', this.name, this.opts);
	
	var StorageClass = require('./storage/' + this.opts.storage );
	this.storage = new StorageClass(this.name, this.opts);
	
	// ### methods
	this.handle = function(func){
		return _this.storage.handle(func);
	};
	
	this.add = function(obj, delay){
		
		var msg = {payload: obj};
		if( delay ){
			msg.delay = delay;
		}
		log('Queue - add - name = %s | obj = %j', _this.name, msg);
		_this.eventEmitter.emit('new', obj);
		
		return _this.storage.add(msg);
		
	};
	
	this.on = function(name, func){
		log('Queue - on - name = %s | event = %s', _this.name, name);
		_this.eventEmitter.on(name, func);
	};
	
};


