
// ## dependencies
var events = require('events');
var log = require('debug')('lean-mq');

var module_queue = require('./queue');

// ## config
var default_window = 5 * 1000; // 5 seconds

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.Chain = function(arg_name, arg_opts){
	
	log('Chain.init - start - name =', arg_name, '| opts =', arg_opts);
	
	// ### props
	var _this = this;
	this.name = 'none';
	
	this.opts = {
		window: default_window,
		storage: 'memory',
		connection: null,
		attempts: 3,
		retry_delay: 1000,
		group: 'time'
	};
	
	this.workers = [];
	this.robin = -1;
	
	this.input = null;
	this.delay = null;
	this.output = null;
	this.table = null;
	
	var emitter = new events.EventEmitter();
	
	// ### config
	// TODO opts validation
	this.name = arg_name;
	
	if( arg_opts ){
		for(var p in this.opts){
			if( typeof(arg_opts[p]) !== 'undefined' ){
				this.opts[p] = arg_opts[p];
			}
		}
	}
	
	if( this.opts.group === 'hash' ){
		this.opts.window = 10;
	}
	
	log('Chain.init - parsed - name =', _this.name, '| opts =', _this.opts);
	
	this.input = new module_queue.Queue(this.name + '-input', this.opts);
	this.delay = new module_queue.Queue(this.name + '-delay', this.opts);
	this.output = new module_queue.Queue(this.name + '-output', this.opts);
	
	var TableStorageClass = require('./storage/' + this.opts.storage + '-table');
	//var TableStorageClass = require('./storage/memory-table');
	this.table = new TableStorageClass(this.name + '-table', this.opts);
	
	this.input.handle(async function(obj){
		try{
			var payload = obj.payload;
			var key = payload.key;
			var val = payload.val;
			log('Chain.input.handle - start - name = %s | key = %s', _this.name, key);
			var found = await _this.table.get( key );
			log('Chain.input.handle - lookup - name = %s | key = %s | found = %s', _this.name, key, found);
			
			if( !found ){
				found = []; // new group
				var date = new Date();
				var sort = {key: key, ts:date.toISOString(), window: _this.opts.window };
				log('Chain.input.handle - new - name = %s | sort = %j', _this.name, sort);
				
				_this.delay.add(sort, _this.opts.window);
			}
			
			found.push( val );
			var res_set = await _this.table.set( key, found );
			log('Chain.input.handle - set - key = %s | set = %s', key, res_set);
			
			return true;
		}
		catch(err){
			log('Chain.input.handle - catch - msg = %s | stack = %s', err.message, err.stack);
			throw err;
		}
	});
	
	this.delay.handle(async function(obj){
		
		var payload = obj.payload;
		var key = payload.key;
		log('Chain.delay.handle - name =', _this.name, '| key =', key);
		
		var final_list = await _this.table.get( key );
		
		_this.output.add({ key: key, data: final_list });
		await _this.table.del( key );
		
	});
	
	this.output.handle(function(obj){
		
		log('Chain.output.handle - start - name =', _this.name, '| robin =', _this.robin, '| obj =', obj);
		
		if( _this.workers.length === 0 ){
			return Promise.reject('no workers attached');
		}
		
		// ## distribute
		// default round robin
		_this.robin += 1;
		// hash
		if( _this.opts.group === 'hash' ){
			//{"payload":{"key":"3","data":[{"smth":"abc"}]}}
			var hash = ( obj.payload.key.length % _this.workers.length );
			//log('DEBUG = %s / %s', obj.payload.key, hash);
			_this.robin = hash;
		}
		
		// ## save from out-of-index
		if( !_this.workers[ _this.robin ] ){
			_this.robin = 0;
		}
		
		log('Chain.output.handle - before func - name =', _this.name, '| robin =', _this.robin);
		return _this.workers[ _this.robin ]( obj ).then(function(result){
			
			return Promise.resolve( true );
			
		}).catch(function(err){
			
			emitter.emit('error', {msg: 'worker rejected', response: err});
			return Promise.reject( false );
		});
		
	});
	
	// ### methods
	this.attach = function( func ){
		_this.workers.push( func );
	};
	this.handle = this.attach; // method compatibility with Queue class
	
	this.add = function(key, obj){
		
		log('Chain - add - name =', this.name, '| key =', key, '| obj =', obj);
		return _this.input.add({ key: key, val: obj });
		
	};
	
	this.on = function(name, func){
		log('Chain - on - name = %s | event = %s', _this.name, name);
		emitter.on(name, func);
	};
	
};


