
// ## dependencies
var express = require('express');

// ## config
var config = {
	PORT: 9009
};

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.init = function(root){
	console.log('dashboard - start - port = %s', config.PORT);
	var server = express();
	server.use(express.json());
	var broker = new root.Broker({});
	
	server.get('/api/info', function(req, res){
		return res.json({ config: broker.opts, topics: broker.topics });
	});
	server.post('/api/topics', async function(req, res){
		console.log('dh.topics.post - start - obj = %j', req.body);
		var name = req.body.name;
		
		if( !broker.topics[name] ){
			await broker.add( name );
		}
		
		return res.json({ topics: Object.keys(broker.topics) });
	});
	
	server.get('*', express.static(root.dir + '/html'));
	
	server.listen(config.PORT, function () {
		console.log('dashboard - started...', );
	});
};


